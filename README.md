# Web Coding Challenge

## Prerequisites

* Docker
* Docker Compose

## Quick Start

### clone the repo with the submodules

```sh
git clone --recursive git@github.com:rasheedhamdawi/web-coding-challenge.git
```

### Add the Environment Variables

Simply copy the `.env.simple` to `.env` and set the variables.

Example:

```
# PRISMA
PRISMA_MANAGEMENT_API_SECRET=my-secret

# POSTGRES
POSTGRES_USER=prisma
POSTGRES_PASSWORD=prisma
```

PS: also you need to set the variables on the frontend and the backend folder.

### Start the full stack application with single command

```
docker-compose up
```

### deploy the service to local server

```
docker-compose run backend yarn deploy
```

wait for a minute and open your browser on `http://localhost:4000/` to see the
graphql playground and `http://localhost:3000/` for the frontend app.
